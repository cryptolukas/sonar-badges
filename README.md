![Gate](https://badges.mldsc.de/mldsc.sonar-badges-alert_status.svg)
![Code_smells](https://badges.mldsc.de/mldsc.sonar-badges-code_smells.svg)
![Security](https://badges.mldsc.de/mldsc.sonar-badges-security_rating.svg)
![Depts](https://badges.mldsc.de/mldsc.sonar-badges-sqale_index.svg)
![Maintainability](https://badges.mldsc.de/mldsc.sonar-badges-sqale_rating.svg)
![Vulnerability](https://badges.mldsc.de/mldsc.sonar-badges-vulnerabilities.svg)

# Sonar Badges

If you are using a SonarQube instance with Authorization.

Unauthorized user cant see the Badges in your README file.

With this project you can do that

### Needed adjustments:

For the application adjust the [environment variables](ansible/sonar-badges/files/docker-compose.yml)
```
AUTH_TOKEN=XXXXXXXXXX
SONAR_SERVER=XXXXXXXXXX
```

How you create a User Token check it out [here](https://docs.sonarqube.org/latest/user-guide/user-token/)

For the deployment use a valid hostname [hosts-file](ansible/hosts)
```
cd ansible && ansible-playbook -D -i hosts site.yml
```
