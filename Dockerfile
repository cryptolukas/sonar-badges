FROM python:3.8

WORKDIR /usr/src/app
RUN mkdir svgs
COPY requirements.txt ./
COPY get_badges.py ./

RUN pip install --no-cache-dir -r requirements.txt

CMD [ "python", "./get_badges.py" ]
