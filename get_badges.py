#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import asyncio
import pathlib
import sys

import aiohttp
import os

INTERVAL = 30
METRICS = ['bugs', 'code_smells', 'coverage', 'duplicated_lines_density',
           'ncloc', 'sqale_rating', 'alert_status', 'reliability_rating',
           'security_rating', 'sqale_index', 'vulnerabilities']
PROJECT_INDEX = '/api/projects/index'


async def fetch(client_session, url, type_):
    async with client_session.get(url) as response:
        return await response.json() if type_ == 'json' else await response.text()


async def fetch_urls(client_session, sonar_server_url):
    project_url = f'{sonar_server_url}{PROJECT_INDEX}'
    urls = []
    projects = await fetch(client_session, project_url, "json")
    for project in projects:
        for metric in METRICS:
            filename = f'{project["k"]}-{metric}.svg'
            urls.append(
                {filename: f'{sonar_server_url}/api/project_badges/measure?project={project["k"]}&metric={metric}'}
            )
    return urls


async def fetch_image(client_session, filename, url):
    image = await fetch(client_session, url, "text")
    svg_dir = pathlib.Path('svg')
    svg_dir.mkdir(exist_ok=True)
    file = svg_dir / filename
    file.open('wb')
    with (pathlib.Path(f'svgs') / filename).open(mode='wb') as file:
        if 'Measure has not been found' not in image and 'errors":[{"msg":"Value of parameter' not in image:
            file.write(image.encode())
        else:
            print(f"Invalid measurement content for {filename}")


async def main(sonar_server_url):
    while True:
        async with aiohttp.ClientSession(auth=aiohttp.BasicAuth(auth_token)) as client_session:
            async with asyncio.Semaphore(20):
                url_list = await asyncio.wait_for(fetch_urls(client_session, sonar_server_url), 1)
                futures = []
                for item in url_list:
                    for filename, url in item.items():
                        futures.append(fetch_image(client_session, filename, url))
                await asyncio.wait(futures)
        await asyncio.sleep(INTERVAL)


if __name__ == '__main__':
    try:
        auth_token = os.environ['AUTH_TOKEN']
        sonar_url = os.environ['SONAR_SERVER']
    except KeyError as e:
        print(f"Missing environment variable {e}")
        sys.exit()
    try:
        loop = asyncio.get_event_loop()
        task = loop.create_task(main(sonar_url))
        loop.run_until_complete(task)
    except (asyncio.CancelledError, KeyboardInterrupt):
        print("unexpected interrupt")
